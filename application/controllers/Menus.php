<?php
  class  Menus extends CI_Controller

  {
    //contructor
    function __construct()
    {
      parent::__construct();
    }
    //renderizacion de la vista de desayunos
    public function desayunos(){
      $this->load->view('header');
      $this->load->view('menus/desayunos');
      $this->load->view('footer');
    }
    //renderizacion de la vista de almuerzos
    public function almuerzos(){
      $this->load->view('header');
      $this->load->view('menus/almuerzos');
      $this->load->view('footer');
    }
    //renderizacion de la vista de meriendas
    public function meriendas(){
      $this->load->view('header');
      $this->load->view('menus/meriendas');
      $this->load->view('footer');
    }

  } //no borrar
 ?>
